const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.use(body.urlencoded({exteded:true}));
app.use(cookie());
app.use(session({
secret:'Passw0rd',
resave:true,
saveUnintialized:true
}));


app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '',
    port: 3306,
    database: 'gitlabhw3'
}, 'single'));

const wut = require('./routes/s611998039Route');
app.use('/', wut);

const loginRoute = require('./routes/loginRoute');
app.use('/', loginRoute);

const customer = require('./routes/s611998006Route');
app.use('/', customer);

const oat = require('./routes/s611998003Route');
app.use('/', oat);

const arm = require('./routes/s611998032Route');
app.use('/', arm);


app.listen(8081);
