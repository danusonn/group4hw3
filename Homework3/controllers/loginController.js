const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');

controller.log = (req, res) => {
    res.render('loginForm', { session: req.session });
};
controller.login = function(req, res) {
    if (req.body.username == 'ce' && req.body.password == 'mirot') {
        req.session.user = req.body.username;
        res.render('/', { session: req.session });
    } else {
      res.redirect('/');
    };
};
controller.logcom = function(req, res) {
    if (req.session.user) {
        res.render('home', { session: req.session });
    } else {
        res.redirect('/');
    }
};
controller.logout = function(req, res) {
    req.session.destroy();
    res.redirect('/');
};


controller.cklogin = (req,res) => {
          const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors = errors;
              req.session.success = false;
              res.redirect('/');
          }else{
              req.session.success=true;

              if (!req.session.user) {
                  req.session.topic="username หรือ password ไม่ถูกต้อง";
              }
              if (req.body.username == 'ce' && req.body.password == 'mirot') {
                  req.session.user = req.body.username;
                  req.session.topic="Login สำเร็จ";
                  res.redirect('/home');
              } else {
                res.redirect('/');
              };
    };
};

module.exports = controller;
