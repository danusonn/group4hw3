
const controller = {};

const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query('SELECT * FROM mirot611998003 ',(err,s611998003) => {
      if(err){
        res.json(err);
      }
    res.render('../views/s611998003/s611998003',{
      data:s611998003,
      session:req.session

    });
    });
  });
};

controller.save = (req,res) => {

    const data=req.body;

          const errors = validationResult(req);
          if(!errors.isEmpty()){
              req.session.errors = errors;
              req.session.success = false;
              res.redirect('/oat/new')
          }else{

              req.session.success=true;
              req.session.topic="เพิ่มข้อมูลสำเร็จ";
              req.getConnection((err,conn)=>{
                  conn.query('INSERT INTO mirot611998003 set ?',[data],(err,s611998003)=>{
              res.redirect('/oat');
              });
          });


    };
};

controller.delete = (req,res) => {
    const { id } = req.params;
      req.getConnection((err,conn) => {
        conn.query('DELETE FROM mirot611998003 WHERE id03 = ?',[id],(err,s611998003) => {
          if(err){
              res.json(err);
          }
          console.log('../views/s611998003/s611998003Delete');
          res.redirect('/oat');
       });
    });
 };

 controller.edit = (req,res) => {
     const { id } = req.params;
       req.getConnection((err,conn) => {
         conn.query('SELECT * FROM mirot611998003 WHERE id03 = ?',[id],(err,s611998003) => {
           if(err){
               res.json(err);
           }
        res.render('../views/s611998003/s611998003Form',{
          data:s611998003[0],
          session:req.session
        });
      });
    });
  };

  controller.update = (req,res) => {
  const { id }=req.params;
  const data=req.body;
  const errors = validationResult(req);
  if(!errors.isEmpty()){
      req.session.errors = errors;
      req.session.success = false;
      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM mirot611998003 WHERE id03 = ?',[id],(err,s611998003) => {
          if(err){
              res.json(err);
          }
       res.render('../views/s611998003/s611998003Form',{
         data:s611998003[0],
         session:req.session
       });
     });
   });
  }else{
      req.session.success=true;
      req.session.topic="เเก้ไขข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('UPDATE mirot611998003 SET ? WHERE id03 = ?',[data,id],(err,s611998003)=>{
          if(err){
            res.json(err);
          }
          res.redirect('/oat');
        });
      });
  }

};

controller.new = (req,res) => {
const data = null;
res.render('../views/s611998003/s611998003Form',{
  data:data,
  session:req.session

});
};


/*const errors = validationResult(req);
if(!errors.isEmpty()){
    req.session.errors = errors;
    req.session.success = false;
    res.redirect('/wut/new')
}else{
    req.session.success=true;
    req.session.topic="เพิ่มข้อมูลสำเร็จ";
    res.redirect('/wut');
}
};*/

controller.del = (req,res) => {
    const { id } = req.params;
      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM mirot611998003 WHERE id03 = ?',[id],(err,s611998003) => {
          if(err){
              res.json(err);
          }
       res.render('../views/s611998003/s611998003Delete',{
         data:s611998003[0],
         session:req.session

       });
     });
   });
 };







module.exports = controller;
