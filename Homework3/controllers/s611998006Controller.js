const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM mirot611998006',(err,s611998006) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998006/s611998006',{session: req.session,data:s611998006});
        });
    });
};

controller.add = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998006/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO mirot611998006 set ?',[data],(err,s611998006)=>{
            res.redirect('/s611998006');
            });
        });
    };
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM mirot611998006 WHERE id06= ?',[id],(err,s611998006)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998006/s611998006Delete',{session: req.session,data:s611998006[0]});
        });
    });
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM mirot611998006 WHERE id06= ?',[id],(err,s611998006)=>{
            if(err){
                res.json(err);
            }
            console.log(s611998006);
            res.redirect('/s611998006');
        });
    });
};

controller.edit = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM mirot611998006 WHERE id06= ?',[id],(err,s611998006)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998006/s611998006Form',{session: req.session,data:s611998006[0]});
        });
    });
};

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM mirot611998006 WHERE id06= ?',[id],(err,s611998006)=>{
                    if(err){
                        res.json(err);
                    }
                    res.render('s611998006/s611998006Form',{session: req.session,data:s611998006[0]});
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  mirot611998006 SET ?  WHERE id06 = ?',[data,id],(err,s611998006) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/s611998006');
               });
             });
        }
}

controller.new = (req,res) => {
    const data = null;
        res.render('s611998006/s611998006Form',{session: req.session,data:data});
};


module.exports = controller;
