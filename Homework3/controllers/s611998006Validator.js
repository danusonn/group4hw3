const { check } = require('express-validator');
//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator
exports.addValidator = [
    check('z611998006',"float ไม่ถูกต้อง ").isFloat(),
    check('y611998006',"varchar ไม่ถูกต้อง ").not().isEmpty(),
    check('x611998006',"int ไม่ถูกต้อง ").isInt(),
    check('w611998006',"date ไม่ถูกต้อง ").isDate(),
    check('v611998006',"time ไม่ถูกต้อง ").not().isEmpty()];
exports.editValidator = [
    check('z611998006',"float ไม่ถูกต้อง ").isFloat(),
    check('y611998006',"varchar ไม่ถูกต้อง ").not().isEmpty(),
    check('x611998006',"int ไม่ถูกต้อง ").isInt(),
    check('w611998006',"date ไม่ถูกต้อง ").isDate(),
    check('v611998006',"time ไม่ถูกต้อง ").not().isEmpty()];
