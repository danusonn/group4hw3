const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM mirot611998032',(err,s611998032) =>{
            if(err){
                res.json(err);
            }
            res.render('s611998032/s611998032',{session: req.session,data:s611998032});
        });
    });
};

controller.add = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998032/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO mirot611998032 set ?',[data],(err,s611998032)=>{
            res.redirect('/s611998032');
            });
        });
    };
};
controller.delete = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM mirot611998032 WHERE id32= ?',[id],(err,s611998032)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998032/s611998032Delete',{session: req.session,data:s611998032[0]});
        });
    });
};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('DELETE FROM mirot611998032 WHERE id32= ?',[id],(err,s611998032)=>{
            if(err){
                res.json(err);
            }
            console.log(s611998032);
            res.redirect('/s611998032');
        });
    });
};

controller.edit = (req,res) => {
    const { id } = req.params;
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM mirot611998032 WHERE id32= ?',[id],(err,s611998032)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998032/s611998032Form',{session: req.session,data:s611998032[0]});
        });
    });
};

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    console.log(data);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM mirot611998032 WHERE id32= ?',[id],(err,s611998032)=>{
                    if(err){
                        res.json(err);
                    }
                    res.render('s611998032/s611998032Form',{session: req.session,data:s611998032[0]});
                });
            });
        }else{
            req.session.success=true;
            req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn) => {
                conn.query('UPDATE  mirot611998032 SET ?  WHERE id32 = ?',[data,id],(err,s611998032) => {
                  if(err){
                      res.json(err);
                  }
               res.redirect('/s611998032');
               });
             });
        }
}

controller.new = (req,res) => {
    const data = null;
        res.render('s611998032/s611998032Form',{session: req.session,data:data});
};


module.exports = controller;
