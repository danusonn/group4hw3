const { check } = require('express-validator');
//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator
exports.addValidator = [
    check('z611998032',"float ไม่ถูกต้อง ").isFloat(),
    check('y611998032',"varchar ไม่ถูกต้อง ").not().isEmpty(),
    check('x611998032',"int ไม่ถูกต้อง ").isInt(),
    check('w611998032',"date ไม่ถูกต้อง ").isDate(),
    check('v611998032',"time ไม่ถูกต้อง ").not().isEmpty()];
exports.editValidator = [
    check('z611998032',"float ไม่ถูกต้อง ").isFloat(),
    check('y611998032',"varchar ไม่ถูกต้อง ").not().isEmpty(),
    check('x611998032',"int ไม่ถูกต้อง ").isInt(),
    check('w611998032',"date ไม่ถูกต้อง ").isDate(),
    check('v611998032',"time ไม่ถูกต้อง ").not().isEmpty()];
