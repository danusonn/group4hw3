const { check } = require('express-validator');

exports.addValidator = [check('z611998039',"FLOATไม่ถูกต้อง!").not().isEmpty(),
                        check('y611998039',"VARCHARไม่ถูกต้อง!").not().isEmpty(),
                        check('x611998039',"INTไม่ถูกต้อง!").not().isEmpty(),
                        check('w611998039',"DATEไม่ถูกต้อง!").not().isEmpty(),
                        check('v611998039',"TIMEไม่ถูกต้อง!").not().isEmpty()];

exports.loginValidator = [check('username', "username ไม่ถูกต้อง").not().isEmpty(),
                        check('password', "password ไม่ถูกต้อง").not().isEmpty()  ];
