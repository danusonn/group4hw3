const express = require('express');
const router = express.Router();

const homeController = require('../controllers/homeControllers');
router.get('/home',homeController.log);
router.post('/home',homeController.log);

module.exports = router;
