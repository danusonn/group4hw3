const express = require('express');
const router = express.Router();

const homeController = require('../controllers/loginController');
const validator = require('../controllers/s611998039Validator');

router.get('/', homeController.log);
router.post('/login', validator.loginValidator, homeController.cklogin);
router.get('/home', homeController.logcom);
router.get('/logout', homeController.logout);








module.exports = router;
