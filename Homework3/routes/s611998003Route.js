const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/s611998003Controllers');

const validator = require('../controllers/s611998003Validator');

router.get('/oat',customerController.list);
router.post('/oat/add',validator.addValidator,customerController.save);
router.get('/oat/delete/:id',customerController.delete);
router.get('/oat/update/:id',validator.addValidator,customerController.edit);
router.post('/oat/update/:id',validator.addValidator,customerController.update);

router.get('/oat/oat2/:id',customerController.del);


router.get('/oat/new',customerController.new);






module.exports = router;
