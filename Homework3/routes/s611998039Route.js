const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/s611998039Controllers');

const validator = require('../controllers/s611998039Validator');

router.get('/wut',customerController.list);
router.post('/wut/add',validator.addValidator,customerController.save);
router.get('/wut/delete/:id',customerController.delete);
router.get('/wut/update/:id',validator.addValidator,customerController.edit);
router.post('/wut/update/:id',validator.addValidator,customerController.update);
/*router.get('/wut/new',customerController.new);*/
router.get('/wut/wut2/:id',customerController.del);


router.get('/wut/new',customerController.new);


router.get('/login',customerController.login);
router.post('/login/log',customerController.form);



module.exports = router;
